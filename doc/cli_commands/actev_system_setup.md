# actev system-setup

## Description

This command will install all the components and dependencies of your system.
All the operations involving a download from the web should be performed in this step.

## Parameters

| Name | Id | Required | Definition                 |
|------|----|----------|----------------------------|
| dev  | d  | False    | If set, disable monitoring |


## Usage

```
actev system-setup [-d/--dev]
```