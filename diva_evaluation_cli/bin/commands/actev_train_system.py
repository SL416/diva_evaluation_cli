"""Actev module: train-system

Actev modules are used to parse actev commands in order to get arguments
before calling associated entry point methods to execute systems.

Warning: this file should not be modified: see src/entry_points to add your
source code.
"""

import logging

from diva_evaluation_cli.bin.commands.actev_command import ActevCommand
from diva_evaluation_cli.src.entry_points.actev_train_system import \
    entry_point


class ActevTrainSystem(ActevCommand):
    """Post-process a chunk.

    Command args:
        * activity-index or a:            activity index JSON file
        * training-data-directory or t:   path to training data directory
    """

    def __init__(self):
        super(ActevTrainSystem, self).__init__(
            'train-system', entry_point)

    def cli_parser(self, arg_parser):
        """Configure the description and the arguments (positional and
        optional) to parse.

        Args:
            arg_parser(:obj:`ArgParser`): Python arg parser to describe how to
                parse the command

        """
        arg_parser.description = "Train the system on surprise activities."
        required_named = arg_parser.add_argument_group(
            'required named arguments')

        required_named.add_argument("-a", "--activity-index", help="path to \
            activity index JSON file", required=True, type=str)
        arg_parser.add_argument("-t", "--training-data-dir", type=str,
                                help="path to training data directory")
        arg_parser.set_defaults(
            func=ActevTrainSystem.command, object=self)
