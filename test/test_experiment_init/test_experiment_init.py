'''
This module implements unit tests of the actev expriment-init command.
'''

import sys
import os
import unittest

from test import test_cli

cdir = os.path.dirname(os.path.abspath(__file__))
res = os.path.join(cdir, "resources")


class TestExperimentInit(unittest.TestCase):
    """
    Test the actev experiment-init command.
    """

    def setUp(self):
        self.file_index = os.path.join(res, "file-index.json")
        self.activity_index = os.path.join(res, "activity-index.json")
        self.chunks = os.path.join(res, "chunks.json")
        self.video_location = os.path.join(res, "videos")
        self.system_cache = os.path.join(cdir, "system_cache")
        self.config = os.path.join(res, "config.yml")

    def test_experiment_init(self):
        sys.argv[1:] = ["experiment-init", "-f", self.file_index, "-a",
                        self.activity_index, "-c", self.chunks, "-v",
                        self.video_location, "-s", self.system_cache]
        test_cli('`actev experiment-init` failed.')

    def test_experiment_init_config(self):
        sys.argv.extend(["-c", self.config])
        test_cli('`actev experiment-init` with config failed.')


if __name__ == '__main__':
    unittest.main()
